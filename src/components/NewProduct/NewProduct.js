import React from "react";
import {useDispatch} from "react-redux";
import {createNewProduct} from "../../store/actions/productsActions";
import ProductForm from "../../containers/ProductForm/ProductForm";

const NewProduct = props => {
    const dispatch = useDispatch();

    const createProduct = productData => {
        dispatch(createNewProduct(productData)).then(() => {
            props.history.push("/");
        });
    };

    return (
        <div className="container">
            <h1 className="text-dark mt-3 text-center">Add new product</h1>
            <ProductForm onSubmit={createProduct} />
        </div>
    );
};

export default NewProduct;