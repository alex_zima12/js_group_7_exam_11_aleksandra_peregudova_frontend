import React from "react";
import {NavLink} from "react-router-dom";

const AnonymousMenu = () => {
    return (
        <>
            <div>
            <NavLink className="font-weight-bold text-light col-2" to={"/register"}>Sign Up</NavLink>
            <NavLink className="font-weight-bold text-light col-2" to={"/login"}>Sign In</NavLink>
            </div>
        </>
    );
};

export default AnonymousMenu;