import React, {useEffect} from 'react';
import AnonymousMenu from "./AnonimousMenu";
import UserMenu from "./UserMenu";
import {NavLink} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {showCategories} from "../../store/actions/categoriesActions";

const Toolbar = (user) => {
    const categories = useSelector(state => state.categories.categories);
    const dispatch = useDispatch();
    console.log(categories, "toolbar categ");

    useEffect(() => {
        dispatch(showCategories());
    }, [dispatch]);

    let categoryItem = categories.map(category => {
        return (
            <NavLink key={category._id} className="nav-link text-light list-group-item active" to={"/" + category._id}>{category.title}</NavLink>
        )
    })
    return (
        <>
            <nav className="navbar navbar-dark bg-primary row">
                <div className="container">
                    <NavLink className="display-4 text-light col-8" to={"/"}>Shop</NavLink>
                    {user.user == null ?
                        <AnonymousMenu />
                        :
                        <UserMenu user={user} />
                    }
                </div>
            </nav>

            <div className="container">
                <ul className="list-group mt-5">
                    {categoryItem}
                </ul>
            </div>
        </>
    );
};

export default Toolbar;