import React from "react";
import {NavLink} from "react-router-dom";
import {useDispatch} from "react-redux";
import {logoutUser} from "../../store/actions/usersActions";

const UserMenu = ({user}) => {
    const dispatch = useDispatch();

    const logout = () => {
        dispatch(logoutUser());
    };

    return user && (
        <>
            <div>
                <p className="font-weight-bold text-light ">Hello,{user.user.displayName}!</p>
                <NavLink className="font-weight-bold text-light " to={"/products/new"}>Add new product</NavLink>
            </div>
            <button onClick={logout}>Logout</button>
        </>
    );
};

export default UserMenu;