import React, {useEffect} from 'react';
import {fetchProductbyCategory} from "../../store/actions/productsActions";
import {useDispatch, useSelector} from "react-redux";
import ProductItem from "../ProductItem/ProductItem";

const SelectedCategory = (props) => {
    const category = props.match.params.category;
    console.log(props.match.params, "пропс");
    const dispatch = useDispatch();
    const products = useSelector(state => state.products.productsByCategory);
    const error = useSelector(state => state.products.error);

    useEffect(() => {
        dispatch(fetchProductbyCategory(category));
    }, [dispatch,category]);

    return (
        <>
            {error ? <b>{error}</b> :
                <div className="container">
                    {products.map(product=> {
                        return (
                            <ProductItem
                                key={product._id}
                                post={product}
                            />
                        )
                    })}
                </div>
            }
        </>
    );
};

export default SelectedCategory;