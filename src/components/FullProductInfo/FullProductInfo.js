import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchFullProductInfo, deleteSelectedProduct} from "../../store/actions/productsActions";
import {apiURL} from "../../constants";
import imageNotAvailable from "../../assets/images/images.png";

const myStyle = {
    width: "200px",
    header: "200px",
    margin: "20px"
};

const FullProductInfo = (props) => {
    const id = props.match.params.id;
    const dispatch = useDispatch();
    const products = useSelector(state => state.products.products);
    const error = useSelector(state => state.products.error);
    const user = useSelector(state => state.users.user);

    let cardImage = imageNotAvailable;
    if (products) {
        cardImage = apiURL + '/uploads/' + products.image;
    }

    useEffect(() => {
        dispatch(fetchFullProductInfo(id));
    }, [dispatch, id]);

    const deleteProduct = () => {
        dispatch(deleteSelectedProduct(id));
    };

    return products && (
        <div className="container">
            {error ? <b>{error}</b> :
                <>
                    <div className="card mt-3">
                        <div style={myStyle}>
                            <img
                                src={cardImage}
                                className="card-img-top"
                                alt="Product"/>
                        </div>
                        <div className="card-body">
                            <h5 className="card-title mt-3">{products.title}</h5>
                            <p>{products.description}</p>
                            <p>{products.price}</p>
                        </div>
                    </div>
                    {!user ? <h2
                            className="text-danger text-center mt-3">
                            Is your item sold? login and delete</h2> :
                        <button
                            type="button"
                            className="btn btn-danger"
                            onClick = {deleteProduct}
                        >
                            Delete
                        </button>
                    }
                </>
            }
        </div>
    );
};

export default FullProductInfo;