import React from 'react';
import {apiURL} from '../../constants';
import imageNotAvailable from "../../assets/images/images.png";
import {NavLink} from "react-router-dom";

const myStyle = {
    width: "100px",
    header: "100px",
    margin: "20px"
};

const ProductItem = (props) => {

    let cardImage = imageNotAvailable ;

    if (props.product.image) {
        cardImage = apiURL + '/uploads/' + props.image;
    }

    return props.product && (
        <div className="card mt-3">
            <div style={myStyle}>
                <img src={cardImage} className="card-img-top" alt="..."/>
            </div>
            <div className="card-body">
                <h1 className="card-title">{props.product.title}</h1>
                <h5 className="card-text">{props.product.price} KGZ </h5>
                <NavLink to={'products/' + props.product._id} className="btn btn-primary">More</NavLink>
            </div>
        </div>
    );
};

export default ProductItem;