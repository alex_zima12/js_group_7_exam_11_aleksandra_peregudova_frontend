import React from 'react';
import Toolbar from "../Navigation/Toolbar";

const Layout = props => {
    return (<>
        <Toolbar user={props.user}/>
        <main>
            {props.children}
        </main>
    </>)
};

export default Layout;