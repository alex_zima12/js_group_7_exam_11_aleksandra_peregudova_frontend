import React from 'react';
import {Switch, Route} from "react-router-dom";
import {useSelector} from "react-redux";
import Layout from "./components/Layout/Layout";
import Login from "./containers/Login/Login";
import Register from "./containers/Register/Register";
import NewProduct from "./components/NewProduct/NewProduct";
import Main from "./containers/Main/Main";
import FullProductInfo from "./components/FullProductInfo/FullProductInfo";
import SelectedCategory from "./components/SelectedCategory/SelectedCategory";


const App = () => {
    const user = useSelector(state => state.users.user);
    const categories = useSelector(state => state.categories.categories);
    console.log(categories, "app");

    let route = categories.map(category => {
              return (
                <Route key={category._id} path={"/" + category._id} exact component={SelectedCategory}/>)

    })
    return (
        <Layout user={user}>
            <Switch>
                <Route path="/" exact component={Main}/>
                <Route path="/products/new" exact component={NewProduct}/>
                <Route path="/products/:id" exact component={FullProductInfo}/>
                <Route path="/register" exact component={Register}/>
                <Route path="/login" exact component={Login}/>
                {route}
                <Route render={() => <h1>404 Not Found</h1>}/>
            </Switch>
        </Layout>
    )
};

export default App;