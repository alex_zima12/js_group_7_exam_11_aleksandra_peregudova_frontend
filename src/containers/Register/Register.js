import React, {useState} from 'react';
import {useDispatch,useSelector} from "react-redux";
import {NavLink} from "react-router-dom";
import {registerUser} from "../../store/actions/usersActions";

const Register = () => {
    const [state, setState] = useState({
        username: "",
        password: "",
        displayName:"",
        phone: ''
    });
    const dispatch = useDispatch();
    const error = useSelector(state => state.users.registerError);
    console.log(error);

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };
    const formSubmitHandler = e => {
        e.preventDefault();
        dispatch(registerUser({...state}));
    }

    let errorMes;
    if (error){
        errorMes = <div className="text-danger mb-3">{error.message}</div>
    }

    return  (
        <form
            onSubmit={formSubmitHandler}
            className="container mt-3"
        >
            <div className="form-group">
                <label htmlFor="InputName">Login</label>
                <input type="text"
                       className="form-control"
                       id="InputName"
                       placeholder="Enter your name"
                       name="username"
                       value={state.username}
                       onChange={inputChangeHandler}
                       required
                />
            </div>
            <div className="form-group">
                <label htmlFor="InputDisplayName">Display Name</label>
                <input type="text"
                       className="form-control"
                       id="InputDisplayName"
                       placeholder="Enter your name"
                       name="displayName"
                       value={state.displayName}
                       onChange={inputChangeHandler}
                       required
                />
                <p htmlFor="InputDisplayName">
                    Adding your Display Name on Shop is a great way to help friends recognize you, login aren't always so easy to understand </p>
            </div>
            <div className="form-group">
                <label htmlFor="InputTel">Phone</label>
                <input type="tel"
                       className="form-control"
                       id="InputTel"
                       placeholder="Enter your phone"
                       name="phone"
                       value={state.phone}
                       onChange={inputChangeHandler}
                       required
                />
            </div>
            <div className="form-group">
                <label htmlFor="InputPassword1">Password</label>
                <input type="password"
                       className="form-control"
                       id="InputPassword1"
                       placeholder="Password"
                       name="password"
                       value={state.password}
                       onChange={inputChangeHandler}
                       required
                />
            </div>
            {errorMes}
            <button type="submit" className="btn btn-primary"> Sign Up</button>
            <div className="mt-3">
            <NavLink to="/login" >
                Already have an account? Sign in
            </NavLink>
            </div>
        </form>
    );
};

export default Register;