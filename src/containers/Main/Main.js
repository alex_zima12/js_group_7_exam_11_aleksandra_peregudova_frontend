import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchProducts} from "../../store/actions/productsActions";
import ProductItem from "../../components/ProductItem/ProductItem";

const Main = () => {

    const products = useSelector(state => state.products.products);
    const error = useSelector(state => state.products.error);
    const dispatch = useDispatch();
    console.log(products, "продукты");

    useEffect(() => {
        dispatch(fetchProducts());
    }, [dispatch]);

    return  (
        <>
            {error ? <b>{error}</b> :
                <div className="container">
                    {products.map(product=> {
                        return (
                            <ProductItem
                                key={product._id}
                                product={product}
                            />
                        )
                    })}
                </div>
            }
        </>
    );
};

export default Main;