import React, {useState} from "react";
import {useSelector} from "react-redux";


const ProductForm = ({onSubmit}) => {

    const categories = useSelector(state => state.categories.categories);

    const [state, setState] = useState({
        title: "",
        description: "",
        price: "",
        image: "",
        category: ""
    });

    const submitFormHandler = e => {
        e.preventDefault();
        let formData = new FormData();
        console.log(state);
        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        });
        onSubmit(formData);
    };
    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };
    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState => ({...prevState, [name]: file}));
    };

    const onChangeSelectHandler = event => {
        const value = event.target.value;
        setState({...state, category: value});
    };

    let select = categories.map(category => {
        return (
            <option key={category._id} value={category._id}>{category.title}</option>
        )
    })

    return (
        <div className="container bg-gradient-info">
            <form
                className="p-5"
                onSubmit={submitFormHandler}
            >
                <div className="input-group mb-3">
                    <input type="text"
                           className="form-control"
                           id="title"
                           placeholder="Title"
                           value={state.title}
                           onChange={inputChangeHandler}
                           name="title"
                           required/>
                </div>
                <div className="input-group mb-3">
                    <textarea className="form-control"
                              placeholder="Write a good product description to sell faster"
                              onChange={inputChangeHandler}
                              name="description"
                              value={state.description}
                              required
                    />
                </div>
                <div className="input-group mb-3">
                    <input type="number"
                           className="form-control"
                           id="price"
                           placeholder="Price"
                           value={state.price}
                           onChange={inputChangeHandler}
                           name="price"
                           required/>
                           <small className="m-3">KGS</small>
                </div>

                <div className="form-group">
                    <label htmlFor="exampleFormControlSelect1">Categories</label>
                    <select
                        className="form-control"
                        id="exampleFormControlSelect1"
                        onChange={onChangeSelectHandler}
                    >
                        {select}
                    </select>
                </div>
                <div className="input-group mb-3 ">
                    <label htmlFor="FormControlFile1">Add file</label>
                    <input
                        type="file"
                        name="image"
                        className="form-control-file"
                        id="FormControlFile1"
                        onChange={fileChangeHandler}
                    />
                </div>
                <button
                    type="submit"
                    className="btn btn-info"
                >Create new product
                </button>
            </form>
        </div>
    );
};

export default ProductForm;