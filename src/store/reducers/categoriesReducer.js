import {
    GET_CATEGORIES_SUCCESS,
    GET_CATEGORIES_FAILURE
} from "../actionTypes";

const initialState = {
    error: null,
    categories: []
};

const categoriesReducer = (state = initialState, action) => {
    switch(action.type) {
        case GET_CATEGORIES_SUCCESS:
            return {...state, categories: action.category};
        case GET_CATEGORIES_FAILURE:
            return {...state, registerError: action.error};
        default:
            return state;
    }
};

export default categoriesReducer;