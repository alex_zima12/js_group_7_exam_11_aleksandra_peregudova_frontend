import {
    FETCH_PRODUCTS_SUCCESS,
    FETCH_PRODUCTS_FAILURE,
    FETCH_FULL_PRODUCT_SUCCESS,
    FETCH_FULL_PRODUCT_FAILURE,
    DELETE_PRODUCT_SUCCESS,
    DELETE_PRODUCT_FAILURE,
    FETCH_PRODUCT_BYCATEGORY_SUCCESS,
    FETCH_PRODUCT_BYCATEGORY_FAILURE
} from "../actionTypes";

const initialState = {
    error: null,
    products: [],
    product: null,
    productsByCategory: [],
};

const productsReducer = (state = initialState, action) => {
    switch(action.type) {
        case FETCH_PRODUCTS_SUCCESS:
            return {...state, products: action.products};
        case FETCH_PRODUCTS_FAILURE:
            return {...state, error: action.error};
        case FETCH_FULL_PRODUCT_SUCCESS:
            return {...state, products: action.product};
        case FETCH_FULL_PRODUCT_FAILURE:
            return {...state, error: action.error};
        case DELETE_PRODUCT_SUCCESS:
            const newProducts = {...state.products};
            const idProduct = newProducts.findIndex(p => p.id === action.id);
            newProducts.splice(idProduct, 1);
            return {...state, products : {...newProducts}};
        case DELETE_PRODUCT_FAILURE:
            return {...state, error: action.error};
        case FETCH_PRODUCT_BYCATEGORY_SUCCESS:
            return {...state, productsByCategory: action.products};
        case FETCH_PRODUCT_BYCATEGORY_FAILURE:
            return {...state, error: action.error};
        default:
            return state;
    }
};

export default productsReducer;