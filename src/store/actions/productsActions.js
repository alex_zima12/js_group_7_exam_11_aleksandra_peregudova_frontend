import {
    CREATE_NEW_PRODUCT_SUCCESS,
    CREATE_NEW_PRODUCT_FAILURE,
    FETCH_PRODUCTS_SUCCESS,
    FETCH_PRODUCTS_FAILURE,
    FETCH_FULL_PRODUCT_SUCCESS,
    FETCH_FULL_PRODUCT_FAILURE,
    DELETE_PRODUCT_SUCCESS,
    DELETE_PRODUCT_FAILURE,
    FETCH_PRODUCT_BYCATEGORY_SUCCESS,
    FETCH_PRODUCT_BYCATEGORY_FAILURE
}
    from "../actionTypes";
import axios from "../../axiosApi";
import {push} from "connected-react-router";



const fetchProductsSuccess = (products) => {
    return {type: FETCH_PRODUCTS_SUCCESS, products};
};

const fetchProductsError = (error) => {
    return {type: FETCH_PRODUCTS_FAILURE, error}
};

export const fetchProducts = () => {
    return async dispatch => {
        try {
            const response = await axios.get("/products");
            dispatch(fetchProductsSuccess(response.data));
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(fetchProductsError(e.response.data));
            } else {
                dispatch(fetchProductsError({global: "No internet"}));
            }
        }
    };
};


const createNewPostSuccess = () => {
    return {type: CREATE_NEW_PRODUCT_SUCCESS};
};

const createNewProductError = (error) => {
    return {type: CREATE_NEW_PRODUCT_FAILURE, error};
};

export const createNewProduct = productData => {
    return async (dispatch, getState) => {
        const headers = {
            'Authorization': getState().users.user && getState().users.user.token
        };
        try {
            await axios.post("/products", productData, {headers}).then(() => {
                dispatch(createNewPostSuccess());
                dispatch(push("/"));
            })
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(createNewProductError(e.response.data));
            } else {
                dispatch(createNewProductError({global: "No internet"}));
            }
        }
    };
};

const fetchFullProductSuccess = (product) => {
    return {type: FETCH_FULL_PRODUCT_SUCCESS, product}
};

const fetchFullProductError = (error) => {
    return {type: FETCH_FULL_PRODUCT_FAILURE, error}
};

export const fetchFullProductInfo = (id) => {
    return async dispatch => {
        try {
            const response = await axios.get("/products/" + id);
            dispatch(fetchFullProductSuccess(response.data));
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(fetchFullProductError(e.response.data));
            } else {
                dispatch(fetchFullProductError({global: "No internet"}));
            }
        }
    };
};

const fetchProductbyCategorySuccess = (products) => {
    return {type: FETCH_PRODUCT_BYCATEGORY_SUCCESS, products}
};

const fetchProductbyCategoryError = (error) => {
    return {type: FETCH_PRODUCT_BYCATEGORY_FAILURE, error}
};

export const fetchProductbyCategory = (category) => {
    return async dispatch => {
        try {
            const response = await axios.get("/products?category=" + category);
            dispatch(fetchProductbyCategorySuccess(response.data));
        } catch (e) {
            if (e.response && e.response.data) {
                dispatch(fetchProductbyCategoryError(e.response.data));
            } else {
                dispatch(fetchProductbyCategoryError({global: "No internet"}));
            }
        }
    };
};


const deleteSelectedProductSuccess = (id) => {
    return {type: DELETE_PRODUCT_SUCCESS, id};
};

const deleteSelectedProductError = (error) => {
    return {type: DELETE_PRODUCT_FAILURE, error};
};

export const deleteSelectedProduct = (id) => {
    return async (dispatch, getState) => {
        try {
            const token = getState().users.user.token;
            const headers = {"Authorization": token};
            await axios.delete("/products/" + id,{headers});
            dispatch(deleteSelectedProductSuccess(id));
            dispatch(push("/"));
        } catch (e) {
            if (e.response && e.response.data) {
                console.log(e);
                dispatch(deleteSelectedProductError (e.response.data));
            } else {
                dispatch(deleteSelectedProductError ({global: "No internet"}));
            }
        }

    };
};

