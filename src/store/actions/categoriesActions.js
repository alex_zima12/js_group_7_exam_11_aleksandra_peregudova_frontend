import {
    GET_CATEGORIES_SUCCESS,
    GET_CATEGORIES_FAILURE}
from "../actionTypes";
import axiosApi from "../../axiosApi";

const getCategoriesSuccess = category => {
    return {type: GET_CATEGORIES_SUCCESS,category};
};
const getCategoriesFailure = error => {
    return {type: GET_CATEGORIES_FAILURE, error};
};

export const showCategories = () => {
    return async dispatch => {
        try {
            const response = await axiosApi.get("/categories");
            dispatch(getCategoriesSuccess(response.data));
        } catch(e) {
            if (e.response && e.response.data) {
                dispatch(getCategoriesFailure(e.response.data));
            } else {
                dispatch(getCategoriesFailure({global: "No internet"}));
            }
        }
    }
};